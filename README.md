# Fog Goof

# >>> https://mazie.rocks/fog-goof/ <<<

![bigmaze.png](https://img.ourl.ca/bigmaze.png)

It is supposed to be a multiplayer version of https://gitlab.com/MRAAGH/fog-maze, but it is very unfinished and currently all it does is display multiple players in different colors.

I have no intentions to work on this project again.

# How to "play"

https://mazie.rocks/fog-goof/

I hope that link is still working when you see this.

If it's not, you can do this:

- download the files game.js index.html maze.js jquery.min.js and style.css
- open the file index.html with your browser (right click and "open with")
