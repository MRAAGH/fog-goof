$(function(){
	var n = 20;
	var visual = new Visual($("#maze"), n);
	visual.adjustSize();
	$(window).resize(function(){visual.adjustSize()});
	var maze = new Maze(n, 5, 5);
	
	var pls = [
		new Player(Math.floor(Math.random()*n*n), hex2Rgb("#FFFF00"), 30),
		new Player(Math.floor(Math.random()*n*n), hex2Rgb("#00FF00"), 30),
		new Player(Math.floor(Math.random()*n*n), hex2Rgb("#FF00FF"), 30),
		new Player(Math.floor(Math.random()*n*n), hex2Rgb("#0000FF"), 30)
	];
	
	visual.clear();
	//visual.displayRaw(maze);
	/*var walls = [];
	for(var i = 0; i < n*n*4; i++)walls[i] = false;
	for(var i = 0; i < n; i++){
		walls[4*n*0    +4*i    +0] = true;
		walls[4*n*(n-1)+4*i    +2] = true;
		walls[4*n*i    +4*0    +1] = true;
		walls[4*n*i    +4*(n-1)+3] = true;
	}*/
	
	var fps = 1;

	gameLoop = function(){
		//Update:
		pls[0].moveRandom(maze.walls, n);
		pls[1].moveRandom(maze.walls, n);
		pls[2].moveRandom(maze.walls, n);
		pls[3].moveRandom(maze.walls, n);
		//Draw:
		visual.clearPlayers(maze.walls, pls);
		pls[0].yx = pls[0].next_yx;
		pls[1].yx = pls[1].next_yx;
		pls[2].yx = pls[2].next_yx;
		pls[3].yx = pls[3].next_yx;
		visual.display(maze.walls, pls);
	};

	//Start the game loop
	setInterval(gameLoop, 100 / fps);
});

function Player(yx, color, view_range){
	this.yx = yx;
	this.next_yx = yx;
	this.base_color_value = color;
	this.base_color = rgb2RgbStr(this.base_color_value);
	this.view_range = view_range;
	this.colors = [];
	this.color_values = [];
	this.midcolors = [this.base_color];
	this.midcolor_values = [this.base_color_value];
	this.prevdir = 5;
	
	//fill this.colors, this.color_codes, this.mid_colors and this.midcolor_codes:
	for(var c = 0; c < this.view_range; c++){
		var mult = (this.view_range-c) / (this.view_range);
		mult *= mult;
		//mult = Math.sqrt(mult);
		this.color_values[c] = [
			Math.floor(this.base_color_value[0]*mult),
			Math.floor(this.base_color_value[1]*mult),
			Math.floor(this.base_color_value[2]*mult)
		];
		this.colors[c] = rgb2RgbStr(this.color_values[c]);
		mult = (this.view_range-c-1/2) / (this.view_range);
		mult *= mult;
		//mult = Math.sqrt(mult);
		this.midcolor_values[c+1] = [
			Math.floor(this.base_color_value[0]*mult),
			Math.floor(this.base_color_value[1]*mult),
			Math.floor(this.base_color_value[2]*mult)
		];
		this.midcolors[c+1] = rgb2RgbStr(this.midcolor_values[c+1]);
	}
	
	this.moveRandom = function(walls, n){
            console.log('rand');
		var dir;
		var i = 0;
		do {
			dir = Math.floor(Math.random()*4);
			i++;
		} while (walls[4*this.yx+dir]
			// also don't allow going back
			|| ((dir+2 === this.prevdir || dir-2 === this.prevdir)
			// unless we already failed too many times
			&& i < 10 ));
		this.prevdir = dir;
		switch (dir) {
			case 0:
				this.next_yx = this.yx - n;
				break;
			case 1:
				this.next_yx = this.yx - 1;
				break;
			case 2:
				this.next_yx = this.yx + n;
				break;
			default:
				this.next_yx = this.yx + 1;
		}
	}
	this.move = function(walls){
	}
}

function hex2Rgb(hex) {
	var rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	return [
		parseInt(rgb[1], 16),
		parseInt(rgb[2], 16),
		parseInt(rgb[3], 16)
	];
}
function componentToHex(c) {
	var hex = c.toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

function rgb2Hex(r, g, b) {
	return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function mixRgb(add, old, weight){
	return [
		(old[0]+add[0]),
		(old[1]+add[1]),
		(old[2]+add[2])
	];
}

function rgbStr2Rgb(rgbStr) {
	var rgb = rgbStr.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	return [
		parseInt(rgb[1]),
		parseInt(rgb[2]),
		parseInt(rgb[3])
	];
}

function rgb2RgbStr(rgb) {
	return "rgb("+rgb[0]+", "+rgb[1]+", "+rgb[2]+")";
}

function Visual(table, n){
	//Pseudo-2D:
	this.table = table;
	this.tds = [];
	this.td_centers = [];
	this.td_hwalls = [];
	this.td_vwalls = [];
	this.occupiers = []; //The number of players touching this spot
	this.occupiers_hwalls = []; //The number of players touching this wall
	this.occupiers_vwalls = [];
	this.center_color_values = []; //Semi-accurate representation of state on-screen, but more accessible
	this.hwall_color_values = [];
	this.vwall_color_values = [];
	this.state = []; //Each search uses this temporarily, overwriting the previous data
	this.state_hwalls = [];
	this.state_vwalls = [];
	this.paint = 1;
	this.h = n;
	this.w = n;
	this.H = 2*n+1;
	this.W = 2*n+1;
	
	//Create every on-screen element (<td>):
	for (var i = 0; i < this.H; i++){
		var row = $("<tr></tr>");
		row.appendTo(table);
		for(var j = 0; j < this.W; j++){
			var col = $("<td></td>");
			col.appendTo(row);
			this.tds[this.H*i+j] = col;
		}
	}
	//Store refs to centers:
	for (var y = 0; y < this.h; y++){
		for (var x = 0; x < this.w; x++){
			this.td_centers[this.w*y+x] = this.tds[this.W*(2*y+1)+(2*x+1)];
		}
	}
	//Store refs to horizontal walls:
	for (var y = 0; y <= this.h; y++){
		for (var x = 0; x < this.w; x++){
			this.td_hwalls[this.w*y+x] = this.tds[this.W*(2*y)+(2*x+1)];
		}
	}
	//Store refs to vertical walls:
	for (var y = 0; y < this.h; y++){
		for (var x = 0; x <= this.w; x++){
			this.td_vwalls[this.w*y+x] = this.tds[this.W*(2*y+1)+(2*x)];
		}
	}
	
	
	this.clear = function(){
		for (var ij = 0; ij < this.H*this.W; ij++){
			this.tds[ij].css("background-color", "black");
			this.occupiers[ij] = 0;
			this.occupiers_hwalls[ij] = 0;
			this.occupiers_vwalls[ij] = 0;
		}
	}
	
	this.resetPaint = function(){
		for (var ij = 0; ij < this.H*this.W; ij++){
			this.state[ij] = 0;
			this.state_hwalls[ij] = 0;
			this.state_vwalls[ij] = 0;
		}
		this.paint = 1;
	}
	
	//Ini state:
	this.resetPaint();
	this.clearPlayers = function(walls, players){
		//Just check whether walls are false. Don't bother checking spaces.
		//Just fill spaces black together with walls.
		//Since There will always be a colored wall leading to a colored space.
		//So check for wall false and wall occupiers > 0
		//Otherwise, a lot like coloring in the forst place.
		//But there is only one paint because, frankly, it doesn't matter.
		//One player clears everything within reach so the areas won't even touch each other.
		//And they can have the same paint.
		//Using depth-first search, because it works just fine in this context. And it is faster.
		//There is no paint! Recognize finished portions by occupier count.
		
		for(var p = 0; p < players.length; p++){
			var yx = players[p].yx;
			this.occupiers[yx] = 0;
			this.td_centers[yx].css("background-color", "black");
			
			//Depth-first search for currently colored squares in this area:
			var stack = [yx];
			do{
				if(!walls[4*yx] && this.occupiers_hwalls[yx] > 0){
					this.occupiers_hwalls[yx] = 0;
					this.td_hwalls[yx].css("background-color", "black");
					this.occupiers[yx-this.w] = 0;
					this.td_centers[yx-this.w].css("background-color", "black");
					stack.push(yx);
					yx = yx-this.w;
					continue;
				}
				if(!walls[4*yx+2] && this.occupiers_hwalls[yx+this.w] > 0){
					this.occupiers_hwalls[yx+this.w] = 0;
					this.td_hwalls[yx+this.w].css("background-color", "black");
					this.occupiers[yx+this.w] = 0;
					this.td_centers[yx+this.w].css("background-color", "black");
					stack.push(yx);
					yx = yx+this.w;
					continue;
				}
				if(!walls[4*yx+1] && this.occupiers_vwalls[yx] > 0){
					this.occupiers_vwalls[yx] = 0;
					this.td_vwalls[yx].css("background-color", "black");
					this.occupiers[yx-1] = 0;
					this.td_centers[yx-1].css("background-color", "black");
					stack.push(yx);
					yx = yx-1;
					continue;
				}
				if(!walls[4*yx+3] && this.occupiers_vwalls[yx+1] > 0){
					this.occupiers_vwalls[yx+1] = 0;
					this.td_vwalls[yx+1].css("background-color", "black");
					this.occupiers[yx+1] = 0;
					this.td_centers[yx+1].css("background-color", "black");
					stack.push(yx);
					yx = yx+1;
					continue;
				}
				yx = stack.pop();
			}while(stack.length > 0);
		}
	}
	
	this.display = function(walls, players){
		//All of this is in pseudo-2D coordinates
		//because I wanted it optimized. I apologize to the reader.
		for(var p = 0; p < players.length; p++){
			
			this.state[players[p].yx] = this.paint;
			//this.occupiers[players[p].yx]++;
			//this.center_color_values[players[p].yx] = [255, 255, 255];
			
			//Breadth-first search for spaces to color in:
			var cur_set = [players[p].yx];
			var head = 0;
			for(var depth = 0; depth < players[p].view_range; depth++){
				//A wild double-buffer pattern appeared!
				//I am using it because I want to make a specific number of depth steps.
				var next_set = [];
				for(var s = 0; s < cur_set.length; s++){
					var yx = cur_set[s];
					//Iterate over linked cells (not iteration as much as copy-pasted code)
					if(!walls[4*yx] && this.state_hwalls[yx]<this.paint){
						this.state_hwalls[yx] = this.paint;
						if(this.occupiers_hwalls[yx] == 0){
							this.hwall_color_values[yx] = players[p].midcolor_values[depth];
							this.td_hwalls[yx].css("background-color", players[p].midcolors[depth]);
						}
						else{
							var mixed_color = mixRgb(players[p].midcolor_values[depth],
								this.hwall_color_values[yx],
								this.occupiers_hwalls[yx]);
							this.hwall_color_values[yx] = mixed_color;
							this.td_hwalls[yx].css("background-color", rgb2RgbStr(mixed_color));
						}
						this.occupiers_hwalls[yx]++;
						if(this.state[yx-this.w]<this.paint){
							next_set.push(yx-this.w);
							this.state[yx-this.w] = this.paint;
							if(this.occupiers[yx-this.w] == 0){
								this.center_color_values[yx-this.w] = players[p].color_values[depth];
								this.td_centers[yx-this.w].css("background-color", players[p].colors[depth]);
							}
							else{
								var mixed_color = mixRgb(players[p].color_values[depth],
									this.center_color_values[yx-this.w],
									this.occupiers[yx-this.w]);
								this.center_color_values[yx-this.w] = mixed_color;
								this.td_centers[yx-this.w].css("background-color", rgb2RgbStr(mixed_color));
							}
							this.occupiers[yx-this.w]++;
						}
					}
					if(!walls[4*yx+2] && this.state_hwalls[yx+this.w]<this.paint){
						this.state_hwalls[yx+this.w] = this.paint;
						if(this.occupiers_hwalls[yx+this.w] == 0){
							this.hwall_color_values[yx+this.w] = players[p].midcolor_values[depth];
							this.td_hwalls[yx+this.w].css("background-color", players[p].midcolors[depth]);
						}
						else{
							var mixed_color = mixRgb(players[p].midcolor_values[depth],
								this.hwall_color_values[yx+this.w],
								this.occupiers_hwalls[yx+this.w]);
							this.hwall_color_values[yx+this.w] = mixed_color;
							this.td_hwalls[yx+this.w].css("background-color", rgb2RgbStr(mixed_color));
						}
						this.occupiers_hwalls[yx+this.w]++;
						if(this.state[yx+this.w]<this.paint){
							next_set.push(yx+this.w);
							this.state[yx+this.w] = this.paint;
							if(this.occupiers[yx+this.w] == 0){
								this.center_color_values[yx+this.w] = players[p].color_values[depth];
								this.td_centers[yx+this.w].css("background-color", players[p].colors[depth]);
							}
							else{
								var mixed_color = mixRgb(players[p].color_values[depth],
									this.center_color_values[yx+this.w],
									this.occupiers[yx+this.w]);
								this.center_color_values[yx+this.w] = mixed_color;
								this.td_centers[yx+this.w].css("background-color", rgb2RgbStr(mixed_color));
							}
							this.occupiers[yx+this.w]++;
						}
					}
					if(!walls[4*yx+1] && this.state_vwalls[yx]<this.paint){
						this.state_vwalls[yx] = this.paint;
						if(this.occupiers_vwalls[yx] == 0){
							this.vwall_color_values[yx] = players[p].midcolor_values[depth];
							this.td_vwalls[yx].css("background-color", players[p].midcolors[depth]);
						}
						else{
							var mixed_color = mixRgb(players[p].midcolor_values[depth],
								this.vwall_color_values[yx],
								this.occupiers_vwalls[yx]);
							this.vwall_color_values[yx] = mixed_color;
							this.td_vwalls[yx].css("background-color", rgb2RgbStr(mixed_color));
						}
						this.occupiers_vwalls[yx]++;
						if(this.state[yx-1]<this.paint){
							next_set.push(yx-1);
							this.state[yx-1] = this.paint;
							if(this.occupiers[yx-1] == 0){
								this.center_color_values[yx-1] = players[p].color_values[depth];
								this.td_centers[yx-1].css("background-color", players[p].colors[depth]);
							}
							else{
								var mixed_color = mixRgb(players[p].color_values[depth],
									this.center_color_values[yx-1],
									this.occupiers[yx-1]);
								this.center_color_values[yx-1] = mixed_color;
								this.td_centers[yx-1].css("background-color", rgb2RgbStr(mixed_color));
							}
							this.occupiers[yx-1]++;
						}
					}
					if(!walls[4*yx+3] && this.state_vwalls[yx+1]<this.paint){
						this.state_vwalls[yx+1] = this.paint;
						if(this.occupiers_vwalls[yx+1] == 0){
							this.vwall_color_values[yx+1] = players[p].midcolor_values[depth];
							this.td_vwalls[yx+1].css("background-color", players[p].midcolors[depth]);
						}
						else{
							var mixed_color = mixRgb(players[p].midcolor_values[depth],
								this.vwall_color_values[yx+1],
								this.occupiers_vwalls[yx+1]);
							this.vwall_color_values[yx+1] = mixed_color;
							this.td_vwalls[yx+1].css("background-color", rgb2RgbStr(mixed_color));
						}
						this.occupiers_vwalls[yx+1]++;
						if(this.state[yx+1]<this.paint){
							next_set.push(yx+1);
							this.state[yx+1] = this.paint;
							if(this.occupiers[yx+1] == 0){
								this.center_color_values[yx+1] = players[p].color_values[depth];
								this.td_centers[yx+1].css("background-color", players[p].colors[depth]);
							}
							else{
								var mixed_color = mixRgb(players[p].color_values[depth],
									this.center_color_values[yx+1],
									this.occupiers[yx+1]);
								this.center_color_values[yx+1] = mixed_color;
								this.td_centers[yx+1].css("background-color", rgb2RgbStr(mixed_color));
							}
							this.occupiers[yx+1]++;
						}
					}
					
					
					
				}
				//"Swap" buffers:
				cur_set = next_set;
			}
			//Next paint.
			this.paint++;
		}
		
		//Players themselves are white:
		for(var p = 0; p < players.length; p++){
			this.td_centers[players[p].yx].css("background-color", "white");
			this.center_color_values[players[p].yx] = [255, 255, 255];
		}
	}
	
	this.displayRaw = function(maze){
		var color1 = "black";
		var color2 = "#101010";
		var colordead = "yellow";
		var colorerr = "red";
		
		//Poles:
		for (var i = 0; i < this.H; i+=2){
			for (var j = 0; j < this.W; j+=2){
				this.tds[this.W*i+j].css("background-color", color1);
			}
		}
		
		//Spaces:
		var call = ["grey", "grey"];
		for(var c = 1; c < 1000; c++){
			call[c] = getRandomColor();
		}
		for (var y = 0; y < maze.h; y++){
			for (var x = 0; x < maze.w; x++){
				var color = call[maze.DEBUGSTATE[maze.h*y+x]];
				color = color2;
				this.tds[this.W*(2*y+1)+(2*x+1)].css("background-color", color);
			}
		}
		
		function getRandomColor() {
			var letters = '3456789ABCDEF';
			var color = '#';
			for (var i = 0; i < 6; i++ ) {
				color += letters[Math.floor(Math.random() * letters.length)];
			}
			return color;
		}
		
		//Horizontal walls:
		for (var y = 0; y <= maze.h; y++){
			for (var x = 0; x < maze.w; x++){
				var color;
				if(y == 0) color = (maze.at(y, x, 0) ? color1 : color2);
				else if(y == maze.h) color = (maze.at(y-1, x, 2) ? color1 : color2);
				else{
					//When there is no edge case, it's possible that the wall is invalid.
					if(maze.at(y, x, 0) != maze.at(y-1, x, 2)) color = colorerr; //(invalid)
					else color = (maze.at(y, x, 0) ? color1 : color2);
				}
				this.tds[this.W*(2*y)+(2*x+1)].css("background-color", color);
			}
		}
		
		//Vertical walls:
		for (var y = 0; y < maze.h; y++){
			for (var x = 0; x <= maze.w; x++){
				var color;
				if(x == 0) color = (maze.at(y, x, 1) ? color1 : color2);
				else if(x == maze.w) color = (maze.at(y, x-1, 3) ? color1 : color2);
				else{
					//When there is no edge case, it's possible that the wall is invalid.
					if(maze.at(y, x, 1) != maze.at(y, x-1, 3)) color = colorerr; //(invalid)
					else color = (maze.at(y, x, 1) ? color1 : color2);
				}
				this.tds[this.W*(2*y+1)+(2*x)].css("background-color", color);
			}
		}
	}
	
	this.adjustSize = function(size){
		var margin = 0;
		var good_ratio = 4/1;
		var PATH = 1;
		var WALL = 1;
		var path = 1;
		var wall = 1;
		while((this.h*path+(this.h+1)*wall < $(window).height()-margin)
		&& (this.w*path+(this.w+1)*wall < $(window).width())-margin){
			PATH = path;
			WALL = wall;
			if(path < wall*good_ratio){
				path++;
			}
			else{
				wall++;
			}
		}
		//PATH -= 2;
		//WALL -= 2;
		//console.log("Chosen: "+PATH+", "+WALL);
		//console.log("P:" + (this.h*PATH+(this.h+1)*WALL)+" p: "+(this.h*path+(this.h+1)*wall)+ " w: " +$(window).height());
		this.table.find("tr:nth-child(odd) > td:nth-child(odd)").css("width", WALL+"px");
		this.table.find("tr:nth-child(odd) > td:nth-child(odd)").css("height", WALL+"px");
		this.table.find("tr:nth-child(odd) > td:nth-child(even)").css("width", PATH+"px");
		this.table.find("tr:nth-child(odd) > td:nth-child(even)").css("height", WALL+"px");
		this.table.find("tr:nth-child(even) > td:nth-child(odd)").css("width", WALL+"px");
		this.table.find("tr:nth-child(even) > td:nth-child(odd)").css("height", PATH+"px");
		this.table.find("tr:nth-child(even) > td:nth-child(even)").css("width", PATH+"px");
		this.table.find("tr:nth-child(even) > td:nth-child(even)").css("height", PATH+"px");
		
		
	}

}

/*
0 up    -y
1 left  -x
2 down  +y
3 right +x
*/















