function Maze(n, deadend_max_length, loop_min_length){
	this.h = n;
	this.w = n;
	this.w4 = n*4;
	//Pseudo-3D (3rd dimension is direction)
	this.walls = [];
	this.at = function(y, x, d){
		return this.walls[this.w4*y+4*x+d];
	}
	
	this.DEBUGSTATE = [];
	for(var yx = 0; yx < this.h*this.w; yx++){
		this.DEBUGSTATE[yx] = 0;
	}
	
	//Generate:
	var grid = new Grid(this.h, this.w);
	{
		//Place all walls:
		for(var yxd = 0; yxd < this.h*this.w*4; yxd++){
			this.walls[yxd] = true;
		}
		
		//Random start:
		var y = Math.floor(Math.random()*this.h);
		var x = Math.floor(Math.random()*this.w);
		var curr = grid.cells[grid.w*y+x];
		var stack = [];
		var backtracking = false;
		var carried_deadend = null;
		do{
			//Mark as visited:
			curr.state = 1;
			
			//Find ways to go:
			var dirs = [];
			for(var d = 0; d < 4; d++){
				if(curr.links[d] != null && curr.links[d].state == 0){
					//d is a valid direction.
					dirs.push(d);
				}
			}
			
			//No matter what's going on with currently valid directions -
			//if we were just carrying a dead end, some management is in place.
			if(carried_deadend != null){
				//Has the dead end reached the treshold length?
				if(carried_deadend.len > deadend_max_length){
					//We can forget about it because it has no effect from here on.
					carried_deadend = null;
				}
				else{
					//No, it's still short.
					//We need to check if another dead end rooted here which would
					//override the one we are carrying, or vice versa.
					if(curr.rooted_deadend != null){
						//Uh oh!
						//Two dead ends join before either of them reaches its full length!
						//Who will get to stay? Well, the longer one, obviously.
						if(carried_deadend.len > curr.rooted_deadend.len){
							//RIP the one which was waiting here.
							curr.rooted_deadend.unlink();
						}
						else{
							//RIP the one we carried up to here.
							//And we'll pick up the other one instead.
							carried_deadend.unlink();
							carried_deadend = curr.rooted_deadend;
						}
						//Either way, only one dead end is left, and we are carrying it.
						//Nothing is rooted any more.
						curr.rooted_deadend = null;
					}
				}
			}
			
			//Are there no valid directions?
			if(dirs.length == 0){
				//Were we already backtracking in the previous iteration?
				if(backtracking){
					//About to backtrack. If we are carrying a dead end, it extends.
					if(carried_deadend != null){
						carried_deadend.len++;
					}
					else{
						//Otherwise, a thing that could happen is that we encounter a root
						//of a dead end because the previous long code blpock was skipped.
						//So we never did check for roots.
						if(curr.rooted_deadend != null){
							//Just pick it up.
							carried_deadend = curr.rooted_deadend;
							curr.rooted_deadend = null;
							//Also, it extends because we are about to backtrack.
							carried_deadend.len++;
						}
					}
				}
				else{
					//No. We JUST started backtracking, mind you. New dead end.
					curr.deadend = new Deadend(curr);
					//Carry it:
					carried_deadend = curr.deadend;
				}
				//Backtrack.
				curr = stack.pop();
				backtracking = true;
			}
			
			else{
				//Yes, there are valid directions.
				//Did we just stop backtracking?
				if(backtracking){
					//Assuming we brought a dead end with us, we leave it here before
					//setting off in another direction. Where's the you that I used to knooooooooow
					if(carried_deadend != null){
						curr.rooted_deadend = carried_deadend;
						carried_deadend = null;
					}
					backtracking = false;
				}
				
				//Choose a direction:
				var dir = dirs[Math.floor(Math.random()*dirs.length)];
				//Dig:
				this.walls[this.w4*curr.y+4*curr.x+dir] = false;
				//Push to stack:
				stack.push(curr);
				//Move:
				curr = curr.links[dir];
				//Dig behind:
				this.walls[this.w4*curr.y+4*curr.x+(dir+2)%4] = false;
			}
		}while(stack.length > 0);
		
		var paint = 2;
		for(var y = 0; y < this.h; y++){
			for(var x = 0; x < this.w; x++){
				var currend = grid.cells[grid.w*y+x];
				//Is this a dead end?
				if(currend.deadend != null){
					
					this.DEBUGSTATE[this.w*y+x] = 1;
					
					//Depth-first search to mark all cells which can
					//be reached with a small number of steps:
					var target = currend.links[dir];
					var curr = currend;
					var stack = [];
					do{
						//First, check if we've gone too far.
						if(stack.length > loop_min_length){
							//Backtrack, and skip everything else.
							curr = stack.pop();
							continue;
						}
						
						//Find one of the available directions for search:
						var dir2 = -1;
						for(var d = 0; d < 4; d++){
							//Must be a non-wall.
							//This silently also checks that it is a cell at all.
							//Because there are walls at the edge of the grid.
							if(!this.walls[this.w4*curr.y+4*curr.x+d]
							&& curr.links[d].state < paint){ //Also, must be a non-visited cell.
								dir2 = d;
								break;
							}
						}
						
						if(dir2 == -1){
							//No directions left. Backtrack.
							curr = stack.pop();
						}
						else{
							//Push current to stack and move on:
							stack.push(curr);
							curr = curr.links[dir2];
							//Mark as visited:
							curr.state = paint;
						}
					}while(stack.length > 0);
					
					//Find potentially valid directions to solve the dead end:
					var dirs = [];
					for(var d = 0; d < 4; d++){
						if(currend.links[d] != null //Must be a cell
						&& this.walls[this.w4*y+4*x+d] //Must be a wall
						&& currend.links[d].state < paint){ //Must not create a small loop
							dirs.push(d);
						}
					}
					//Are there even any directions?
					if(dirs.length == 0){
						//No. This dead end can not be opened.
						//Now would be the time to do some backtracking, maybe try to
						//open it there, possibly find another small branch of the maze...
						//But honestly, I don't feel like bothering with that.
						//Just give up.
						currend.deadend = null;
						break;
					}
					
					//Choose one.
					var dir = -1;
					//If there's another dead end right next to this one, choose that.
					for(var dd = 0; dd < dirs.length; dd++){
						if(currend.links[dirs[dd]].deadend != null){
							//There is, in fact, a dead end right next to this one.
							dir = dirs[dd];
						}
					}
					//Choose a random one. If we haven't decided yet, that is.
					if(dir < 0){
						dir = dirs[Math.floor(Math.random()*dirs.length)];
					}
					
					//Open dead end.
					var other_side = currend.links[dir];
					this.walls[this.w4*y+4*x+dir] = false;
					this.walls[this.w4*other_side.y+4*other_side.x+(dir+2)%4] = false;
					currend.deadend = null;
					//And remove the dead end on the other side if there happens to be one.
					other_side.deadend = null;
					
					//This paint is now spilled across the grid. Choose next one.
					paint++;
				}
			}
		}
		
		for(var yx = 0; yx < this.h*this.w; yx++){
			//if(this.DEBUGSTATE[yx] == 0){
				this.DEBUGSTATE[yx] = grid.cells[yx].state;
			//}
		}
		
	}
	
	function Deadend(end){
		this.end = end;
		this.len = 1;
		this.unlink = function(){this.end.deadend = null;}
	}
	
	function Cell(y, x){
		this.y = y;
		this.x = x;
		this.state = 0;
		this.links = [];
		for(var d = 0; d < 4; d++){
			this.links[d] = null;
		}
		this.deadend = null;
		this.rooted_deadend = null;
	}
	
	function Grid(h, w){
		this.h = h;
		this.w = w;
		this.cells = [];
		//Instantiate cells:
		for(var y = 0; y < this.h; y++){
			for(var x = 0; x < this.w; x++){
				this.cells[this.w*y+x] = new Cell(y, x);
			}
		}
		//Link cells:
		for(var y = 0; y < this.h; y++){
			for(var x = 0; x < this.w; x++){
				if(y > 0)        this.cells[this.w*y+x].links[0] = this.cells[this.w*(y-1)+(x)];
				if(x > 0)        this.cells[this.w*y+x].links[1] = this.cells[this.w*(y)+(x-1)];
				if(y < this.h-1) this.cells[this.w*y+x].links[2] = this.cells[this.w*(y+1)+(x)];
				if(x < this.w-1) this.cells[this.w*y+x].links[3] = this.cells[this.w*(y)+(x+1)];
			}
		}
	}
}













